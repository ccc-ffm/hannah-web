class HannahAPIException(Exception):
    PERMANENT = False
 
class PermanentHannahAPIException(HannahAPIException):
    PERMANENT = True
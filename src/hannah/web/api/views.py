
from rest_framework import viewsets
from hannah.web.api.serializers import MQTTSerializer, UserSerializer, GroupSerializer
from django.contrib.auth.models import User, Group
from rest_framework.response import Response

from hannah.web.api import HannahAPIException


from models import mqtt_client

from rest_framework.views import exception_handler


def api_exception_handler(exc, context):

    if isinstance(exc ,HannahAPIException):
        if exc.PERMANENT:
            status = 500
        else:
            status = 503
        response = Response({'message':exc.message}, status=status, exception=exc)
        return response
    
    # Call REST framework's default exception handler 
    response = exception_handler(exc, context)
    return response



class MqttBaseViewSet(viewsets.ViewSet):
    """
    API endpoint to list and retrieve mqtt topics and their values
    """   
    BASE = r'mqtt/'
       
    def topics(self):
        pass
         
    def topic(self, topic):
        pass
    
    def list(self, request):
        serializer = MQTTSerializer(self.topics(), many=True, context={'request': request}, view_name=self.BASE + '-detail')
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        serializer = MQTTSerializer(self.topic(pk), context={'request': request}, view_name=self.BASE + '-detail')
        return Response(serializer.data)
    
    

class MqttRegisteredViewSet(MqttBaseViewSet):
    """
    API endpoint to list and retrieve mqtt topics and their values
    """   
    BASE = MqttBaseViewSet.BASE + 'registered'
    def topics(self):
        return mqtt_client.get_registered_topics()
    def topic(self, topic):
        return mqtt_client.get_registered_topic(topic)


class MqttSysViewSet(MqttBaseViewSet):
    """
    API endpoint to list and retrieve mqtt topics and their values
    """   
    BASE = MqttBaseViewSet.BASE + 'system'
    def topics(self):
        return mqtt_client.get_sys_topics()
    def topic(self, topic):
        return mqtt_client.get_sys_topic(topic)
    

class MqttTopicViewSet(MqttBaseViewSet):
    """
    API endpoint to list and retrieve mqtt topics and their values
    """   
    BASE = MqttBaseViewSet.BASE + 'topics'
    def topics(self):
        return mqtt_client.get_topics()
    def topic(self, topic):
        return mqtt_client.get_topic(topic)

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


import paho.mqtt.client as mc
import urllib
from datetime import datetime
from _socket import socket

from hannah.web.api import HannahAPIException, PermanentHannahAPIException

REGISTRY_TOPIC = 'registry/'

class mqtt_message(object):
    
    def __init__(self,message):
        self.topic = message.topic
        self.pk = urllib.quote_plus(message.topic)
        self.payload = message.payload
        self.retained = message.retain
        self.event_time = None
        if not self.retained:
            self.event_time = datetime.now()
        
class registered_topic(mqtt_message):
    
    def __init__(self,message):
        super(registered_topic, self).__init__(message)
        self.topic = message.topic[len(REGISTRY_TOPIC):]
        self.pk = urllib.quote_plus(self.topic)
        self.meta = message.payload
 
class ConnectionException(HannahAPIException):
    pass

class PermanentConnectionException(PermanentHannahAPIException):
    pass


class mqtt():
    
    
    def check_connection(self):
        if self.mqttc.socket() is None:
            raise PermanentConnectionException("No Socket")
        if not self.connected:
            raise ConnectionException('No Connetcion established')
    
    def get_topics(self):
        self.check_connection()
        return self.data.values()
    
    def get_topic(self,topic):
        self.check_connection()
        return self.data[topic]
    
    def get_sys_topics(self):
        self.check_connection()
        return self.systopic.values()
    
    def get_sys_topic(self,topic):
        self.check_connection()
        return self.systopic[topic]
    
    def get_registered_topics(self):
        self.check_connection()
        return self.registeredtopics.values()
    
    def get_registered_topic(self,topic):
        self.check_connection()
        return self.registeredtopics[topic]
    
    def on_message(self,mosq,userdata,message):
        m = mqtt_message(message)
        self.data[m.pk] = m
    
    def on_systopic(self,mosq,userdata,message):
        m = mqtt_message(message)
        self.systopic[m.pk] = m
    
    def on_registertopic(self,mosq,userdata,message):
        m = registered_topic(message)
        self.registeredtopics[m.pk] = m
        
    def on_disconnect(self, client, userdata, rc):
        self.connected = False
        print("Unexpected disconnection." + rc.__str__())

    def on_log(self,client, userdata, level, buf):
        pass
        #print buf

    def on_connect(self, client, userdata, flags, rc):
        self.connected = True
        print("Connected with result code "+str(rc))
        client.subscribe('#', 0)
        client.subscribe('$SYS/#')
    
    def __init__(self):
        self.connected = False
        self.data = {}
        self.systopic = {}
        self.registeredtopics = {}
        
        self.mqttc = mc.Client()
        
        
        self.mqttc.on_message = self.on_message 
        self.mqttc.on_disconnect = self.on_disconnect  
        self.mqttc.on_log = self.on_log
        self.mqttc.on_connect = self.on_connect
        self.mqttc.message_callback_add('$SYS/#', self.on_systopic)
        self.mqttc.message_callback_add('registry/#', self.on_registertopic)
        self.mqttc.connect_async('10.21.23.142',keepalive=60,)
        self.mqttc.loop_start()
        
        
        

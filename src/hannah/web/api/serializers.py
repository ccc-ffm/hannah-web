from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.fields import empty

class MQTTSerializer(serializers.Serializer):
        
    def __init__(self, instance=None, data=empty, **kwargs):
        self.view_name = kwargs.pop('view_name')
        serializers.Serializer.__init__(self, instance=instance, data=data, **kwargs)
        
    def get_fields(self):
        url = serializers.HyperlinkedIdentityField(
        view_name=self.view_name,lookup_field='pk'
        )
        return {'url':url, 'topic': serializers.CharField(), 'payload': serializers.CharField(), 'retained':serializers.BooleanField(),'event_time':serializers.DateTimeField()}
    
    
    
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
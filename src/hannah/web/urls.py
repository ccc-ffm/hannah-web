from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from hannah.web.api import views


class ApiRouter(routers.DefaultRouter):
    
    def get_default_base_name(self, viewset):
        if issubclass(viewset, views.MqttBaseViewSet):
            return viewset.BASE
        else:
            return super(ApiRouter, self).get_default_base_name(viewset)

router = ApiRouter()
router.register(views.MqttRegisteredViewSet.BASE , views.MqttRegisteredViewSet)
router.register(views.MqttTopicViewSet.BASE, views.MqttTopicViewSet)
router.register(views.MqttSysViewSet.BASE,views.MqttSysViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hannah_core.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)

from django.contrib import admin

# Register your models here.
from hannah.web.hq.models import Door
admin.site.register(Door)

from hannah.web.hq.models import Room
admin.site.register(Room)

from hannah.web.hq.models import Location
admin.site.register(Location)


from django.db import models
 
class Location(models.Model):
    name = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.name
    

class Room(models.Model):
    name = models.CharField(max_length=100)
    location = models.ForeignKey(Location)
    
    def __unicode__(self):
        return self.name
 
class Door(models.Model): 
    name = models.CharField(max_length=100) 
    locked = models.BooleanField(default=False)
    outside = models.BooleanField(default=False)
    room = models.ForeignKey(Room)
    
    def __unicode__(self):
        return self.name
    
